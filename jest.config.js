module.exports = {
  verbose: true,
  clearMocks: true,
  testEnvironment: 'node',
  setupFilesAfterEnv: ['regenerator-runtime/runtime'],
  testPathIgnorePatterns: [
    "/node_modules/",
  ],
};
