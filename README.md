# OER Wizard

With this planning tool, you can generate an individual guide in five steps, which supports you in creating high-quality educational materials in OER standard. According to your selection, you will receive formal and didactic recommendations as well as information on creative commons licensing details. The click-tool consists of four sections and supports with tips for the creation of small teaching material (e.g. illustrations, presentation slides, video) and task-oriented teaching material (e.g. script, worksheet quizz. The guide can be downloaded as PDF. Tips for the lesson and course guide have yet to be created.


link to [Wizard](https://tibhannover.gitlab.io/oer/oer-wizard/html/wizard-modal.html)

## Installation

Checking out the sources

git clone `https://gitlab.com/TIBHannover/oer/oer-wizard.git`



Change to the project directory

cd ~/oer-wizard/wizard/html
-   open `wizard-modal.html` with browser

## Technologies
- jQuery (SmartWizard)
- bootstrap
- JavaScript
- pdfmake (library)
- css stylesheet


## how can I create wizards or change their content

- If you want to create a new wizard or change their content you must  perform the following operations:
    - clone wizard project from GitLab, see installation
    - go to the "text" directory
    - open the file **object_1.js**:  there are four different keys (**heading, intro, tool, dataformat**)  the values of this key can be changed for example: "**heading**": "_Individueller Leitfaden zur Erstellung von Lehrtexten_" chnage to "**heading**": "your text"
- if you want to change a value of each licences perform the following operations:
    - go to the "text/license" directory
    - open the file **license.js** : the values of "**copyright**" and "**licenses**" can be changed for example: "**copyright_1**" = "_Ihr Material wird ausschließlich aus eigenen Inhalten bestehen_" change to "**copyright_1**" = "your text"

