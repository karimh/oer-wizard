/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var text_1 = "</br>Los geht´s</br></br> Dieser Leitfaden enthält Hinweise für die Erstellung von offenen Bildungsmaterialien. Sie erhalten allgemeine Hinweise zur formalen und didaktischen Gestaltung, zu geeigneten Werkzeugen und Dateiformaten sowie Hinweise zur Lizenzierung. Die Angaben erfolgen auf Basis Ihrer Auswahl. Sollten Sie im Erstellungsprozess davon abweichen, dann prüfen Sie im Falle der Verwendung fremder Materialien bitte erneut die rechtlichen Bedingungen.";

var text_2 = "Bei der Erstellung von OER sollten Sie im Vorfeld überlegen, ob Sie Ihr Material komplett neu oder auf Grundlage bestehender Materialien erstellen möchten. Im Falle der Verwendung fremder Inhalte müssen Sie prüfen, ob Nutzungsrechte vorliegen.";

var text_3 = "Wählen Sie Programme oder Editoren, mit denen Sie gute Erfahrungen haben. Es ist von Vorteil, wenn Sie möglichst freie oder weitverbreitete Programme und Tools nutzen, sodass Nutzer*innen ohne hohen Kosten- und Zeitaufwand Zugang zu Ihrem Material erhalten und es nachnutzen und ggf. weiterbearbeiten können.";

var text_4 = "Konzipieren Sie das Material so übersichtlich, dass es lernfördernd und motivierend ist. Hier finden Sie einige Hinweise zur Gestaltung zu dem von Ihnen gewählten Format.";

var text_5 = "Der Lizenzhinweis schafft die Grundlage für die Nutzung, Bearbeitung und Weiterverbreitung durch Nachnutzer*innen. Daher ist die vollständige Angabe des Lizenztextes am Material oder innerhalb des Materials sehr wichtig. Klare Lizenzhinweise schaffen Vertrauen und Sicherheit und fördern die Verwendung des Materials.";

var text_6 = "Stellen Sie Ihre Bildungsmaterialien in einem möglichst offenen Format bereit, indem Sie leicht versionierbare und editierbare Dateiformate beim Speichern wählen. So können Inhalte einfacher von anderen Lehrenden nachgenutzt werden. Eine Übersicht über geeignete Dateiformate erhalten Sie zeitnah hier.";

var text_7 = "Hier finden Sie verschiedene Informationen und Angebote zum Thema „OER erstellen“.";
