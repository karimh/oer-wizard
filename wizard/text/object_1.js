/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var b1_1_array = {"b1_1": "Lehrtext",
		"heading": "Individueller Leitfaden zur Erstellung von Lehrtexten",

		"intro": "Aufbereitete Lehrinhalte werden so dargestellt, dass sie von Lernenden selbstständig erarbeitet werden können. Eine inhaltliche und typografische Gestaltung von Lehrtexten kann den Prozess des Wissenserwerbs zusätzlich unterstützen. <a href=\"https://franz1ska.gitlab.io/lehrtexte-erstellen/index.html/\" target=\"_blank\" rel=\"noopener noreferrer\">mehr zu Lehrtexte</a>." +
				"\n\n<br><br><h4><strong>Gestaltungshinweise:</strong></h4>\n<ul>"+
				"<li>\n&#8226;	Wählen Sie eine eindeutige Überschrift (auch für die Auffindbarkeit im OER-Portal)." +
				"<li>\n&#8226;	Veranschaulichen Sie in einem Überblick die thematischen Schwerpunkte und bringen Sie diese in einen Zusammenhang." +
				"<li>\n&#8226;	Gliedern Sie den Inhalt in nachvollziehbare Abschnitte." +
				"<li>\n&#8226;	Heben Sie zentrale Aspekte über typographische Elemente hervor." +
				"<li>\n&#8226;	Bauen Sie Exkurse oder Beispiele zur Veranschaulichung ein." +
				"<li>\n&#8226;	Schließen Sie thematische Abschnitte mit einer kurzen Zusammenfassung."+
				"<li>\n&#8226;	Beschriften Sie verwendete Abbildungen eindeutig."+
				"<li>\n&#8226;	Achten Sie auf eine barrierearme Gestaltung (Kontrast, Farben, Schriftgröße).</ul>\n\n",

		"tool": "\nGeeignete Texteditoren für OER sind:<br> Libre Office, MS Office, OnlyOffice, LaTEX, Gitlab.\n ",

		"dataformat": "\nGeeignete Formate sind je nach Texteditor:" +
				"<br>docx, odt, tex, epub für webbasierte Darstellungen md, html und tex.\n"};

var b1_2_array = {"b1_1": "Präsenzaufzeichnung",
		"heading": "Individueller Leitfaden zur Erstellung von Präsenzaufzeichnungen",

		"intro": "Bei Präsenzaufzeichnungen ergeben sich einige Vorteile, nicht nur für Studierende, die Inhalte für Wiederholungszwecke abrufen können, sondern auch für die Gestaltung von Lehrszenarien. Die Aufnahme wird entweder direkt bereitgestellt oder im Nachgang bearbeitet. Im Mittelpunkt steht der Vortrag der sprechenden Person.\n" +
				"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n" +
				"<ul><li>\n&#8226;	Informieren Sie die Studierenden über das Aufzeichnungsvorhaben.</li>" +
				"<li>\n&#8226;	Bestimmen Sie im Vorfeld der Aufnahme den Aufzeichnungsbereich (soll z. B. das Tafelbild oder Whiteboard zu sehen sein?)." +
				"<li>\n&#8226;	Sorgen Sie für eine gute Aufnahmequalität (Bild und Ton)." +
				"<li>\n&#8226;	Fügen Sie eine Navigationsmöglichkeit ein und verschlagworten Sie die Sequenzen zur leichteren Ansteuerung." +
				"<li>\n&#8226;	Ergänzen Sie ggf. einen Vor- und Nachspann. Hier können Sie auch den Titel der Vorlesung und ggf. die Lizenzhinweise sichtbar angeben.</li></ul>",

		"tool": "Für die Aufnahme benötigen Sie  <br>\n" +
				"<ul><li>\n&#8226;	Aufnahmegerät (Digitalkamera, Aufnahmesystem); Speichern Sie die Originaldatei immer in Originalgröße (full HD)</li>" +
				"<li>\n&#8226;	Mikrofon</li>" +
				"<li>\n&#8226;	Videobearbeitungssoftware</li>" +
				"<li>\n&#8226;	Kompressionsprogramm</li></ul>",

		"dataformat": "Geeignete Formate für Videos sind:<br>\n" +
				"<ul><li>\n&#8226;	MP4/H.264 (p)</li>" +
				"<li>\n&#8226;	MP4/H.265 (p)</li>" +
				"<li>\n&#8226;	WEBM/VPx</li>" +
				"<li>\n&#8226;	AVIF/AV1</li></ul>"};

var b1_3_array = {"b1_1": "Videopodcast",
		"heading": "Individueller Leitfaden zur Erstellung von Postcasts",
		"intro": "In einem Videopodcast wird ein spezifischer und möglichst abgeschlossener Themenschwerpunkt behandelt. Das Sprecherbild wird entweder im Office Setting oder in einem Studio aufgezeichnet und steht im Zentrum des Videos. Es können weitere Hilfsmittel zur Erklärung von Sachverhalten hinzugezogen werden (Flipchart, Tafel oder andere Artefakte).\n" +
				"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
				"<li>\n&#8226;	Erstellen Sie ein Drehbuch oder ein Sprechskript (Grob- und Feinstruktur: z.B. Vorspann, Einstieg , Zusammenfassung, Nachspann)" +
				"<li>\n&#8226;	Verzichten Sie auf zusätzliche Informationen, die nicht unmittelbar zum Inhalt gehören." +
				"<li>\n&#8226;	Wählen Sie einen präzisen Titel (auch für die Auffindbarkeit im OER-Portal)." +
				"<li>\n&#8226;	Achten Sie auf eine maximale Dauer von 20 Minuten." +
				"<li>\n&#8226;	Achten Sie bei den eingesetzten Materialien auf eine barrierearme Gestaltung (Kontrast, Farben, Schriftgröße)." +
				"<li>\n&#8226;	Sorgen Sie für eine gute Aufnahmequalität (Bild und Ton) und eine gute Belichtung bei der Aufnahme.</ul>\n\n",

		"tool": "Für die Aufnahme benötigen Sie:<br>\n" +
		"<ul><li>\n&#8226;	Aufnahmegerät (Kamera, PC (Laptop mit Webcam)); Speichern Sie die Originaldatei immer in Originalgröße (full HD)</li>" +
		"<li>\n&#8226;	Mikrofon</li>" +
		"<li>\n&#8226;	Tafel/Whiteboard, Flipchart</li>" +
		"<li>\n&#8226;	Videobearbeitungssoftware" +
		"<li>\n&#8226;	Kompressionsprogramm</li></ul>",

		"dataformat": "Geeignete Formate für Videos sind:<br>\n" +
		"<ul><li>\n&#8226;	MP4/H.264 (p)</li>" +
		"<li>\n&#8226;	MP4/H.265 (p)</li>" +
		"<li>\n&#8226;	WEBM/VPx</li>" +
		"<li>\n&#8226;	AVIF/AV1</li></ul>"};

var b1_4_array = {"b1_1": "Slidecast",
		"heading": "Individueller Leitfaden für die Erstelleung von Slidecasts",
		"intro": "Bei einem Slidecast werden mit einem Präsentationsprogramm gestaltete Folien zeitgleich mit aufgenommenen Audiokommentaren abgespielt.\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Der Foliensatz ist vollständig und fehlerfrei. Eine Folie für den Vorspann sowie eine für den Nachspann rahmen Sie die Aufnahme." +
		"<li>\n&#8226;	Achten Sie bei den Folien auf eine möglichst barrierearme Gestaltung (Kontrast, Farben, Schriftgröße)." +
		"<li>\n&#8226;	Für die Aufnahme empfiehlt sich ein Sprecherskript zu schreiben. Beachten Sie, dass dieses in Sprechsprache formuliert ist." +
		"<li>\n&#8226;	Explizieren Sie eingangs die Lehrziele und den Ablauf (Gliederung)." +
		"<li>\n&#8226;	Achten Sie auf eine maximale Dauer von 15-20 Minuten." +
		"<li>\n&#8226;	Ergänzen Sie ggf. einen Vor- und Nachspann. Hier können Sie auch den Titel der Vorlesung und ggf. die Lizenzhinweise sichtbar angeben." +
		"<li>\n&#8226;	Sorgen Sie für eine gute Aufnahmequalität (Bild und Ton).</ul>\n\n",

		"tool": "Für die Aufnahme benötigen Sie:<br>\n" +
		"<ul><li>\n&#8226;	PC/Laptop)</li>" +
		"<li>\n&#8226;	Mikrofon</li>" +
		"<li>\n&#8226;	Präsentationsprogramm</li>" +
		"<li>\n&#8226;	Screenrecordingprogramm oder PowerPoint" +
		"<li>\n&#8226;	Kompressionsprogramm</li></ul>" +
		"\n Sie können neben der Bereitstellung des Videos auch Ihre Präsentationsfolien in einer Sammlung im OER-Portal Niedersachsen bereitstellen.",

		"dataformat": "Geeignete Formate für Videos sind:<br>\n" +
		"<ul><li>\n&#8226;	MP4/H.264 (p)</li>" +
		"<li>\n&#8226;	MP4/H.265 (p)</li>" +
		"<li>\n&#8226;	WEBM/VPx</li>" +
		"<li>\n&#8226;	AVIF/AV1</li></ul>"};

var b1_5_array = {"b1_1": "Screencast",
		"heading": "Individueller Leitfaden zur Erstellung von Screencasts",
		"intro": "Häufig werden mit einem Screencast Videotutorials realisiert, die die Bedienung von Software zeigen. Die Screencast-Technik eignet sich, wenn z. B. die Dateneingabe in einem Dokument oder Folien bzw. digitale Bilder gezeigt werden sollen.\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Für die Aufnahme empfiehlt es sich ein Sprecherskript zu schreiben (Grobstruktur).Der Text sollte frei gesprochen werden." +
		"<li>\n&#8226;	Explizieren Sie zunächst das Thema, die Lernziele und die erwünschten Lernergebnisse" +
		"<li>\n&#8226;	Achten Sie auf eine angemessene Stoffmenge und Komplexität" +
		"<li>\n&#8226;	Achten Sie auf eine maximale Dauer von 5 Minuten" +
		"<li>\n&#8226;	Rahmen Sie die Aufnahme ggf. durch einen Vorspann und Nachspann" +
		"<li>\n&#8226;	Achten Sie beim Einsatz von weiteren Medienelementen auf eine barrierefreie Gestaltung (Kontrast, Farben, Schriftgröße)" +
		"<li>\n&#8226;	Sorgen Sie für eine gute Aufnahmequalität</ul>\n\n",

		"tool": "Für die Aufnahme benötigen Sie:<br>\n" +
		"<ul><li>\n&#8226;	PC/Laptop)</li>" +
		"<li>\n&#8226;	Mikrofon</li>" +
		"<li>\n&#8226;	ggf. Präsentationsprogramm</li>" +
		"<li>\n&#8226;	Screenrecordingprogramm" +
		"<li>\n&#8226;	Kompressionsprogramm</li></ul>",

		"dataformat": "Geeignete Formate für Videos sind:<br>\n" +
		"<ul><li>\n&#8226;	MP4/H.264 (p)</li>" +
		"<li>\n&#8226;	MP4/H.265 (p)</li>" +
		"<li>\n&#8226;	WEBM/VPx</li>" +
		"<li>\n&#8226;	AVIF/AV1</li></ul>"};

var b1_6_array = {"b1_1": "Video-Aufnahmen",
		"heading": "Individueller Leitfaden zur Erstellung Videoaufnahmen",
		"intro": "Es gibt verschiedene Kontexte, bei denen Videoaufnahmen zu Demonstrationszwecken zum Einsatz kommen können. So können etwa Versuche bzw. Experimente mit der Kamera aufgezeichnet werden und die Aufnahmen im Anschluss ggf. nachbearbeitet werden (z. B. Zeitraffer). Aber auch Aufnahmen von Handlungen oder Vorgängen können als Visualisierungsmöglichkeiten dienlich sein. Eine weitere besondere Aufnahme-Technik stellt die Lege-und Schiebe-Technik dar: Auf einer Arbeitsfläche werden ausgewählte Objekte (häufig ausgeschnittene Formen aus Pappe) platziert und bewegt. Die Videoaufnahme erfolgt nicht zeitgleich mit der Tonaufnahme.\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Filmen Sie zunächst die Sequenzen und zeichnen Sie dann die Audiokommentare auf (Legetechnik)." +
		"<li>\n&#8226;	Erstellen Sie ein Drehbuch, indem Sie die Bilder mit den dazu gehörigen Aussagen festhalten." +
		"<li>\n&#8226;	Achten Sie auf eine maximale Dauer von 5 Minuten." +
		"<li>\n&#8226;	Rahmen Sie die Aufnahme ggf. durch einen Vor- und Nachspann." +
		"<li>\n&#8226;	Achten Sie bei den eingesetzten Materialien auf eine barrierearme Gestaltung (Kontrast, Farben, Schriftgröße)." +
		"<li>\n&#8226;	Sorgen Sie für eine gute Aufnahmequalität (Bild und Ton) und für eine gute Belichtung und Schattenvermeidung bei der Aufnahme.</ul>\n\n",

		"tool": "Für die Aufnahme benötigen Sie:<br>\n" +
		"<ul><li>\n&#8226;	Aufnahmegerät (Kamera, PC/Laptop mit Webcam); Speichern Sie die Originaldatei immer in Originalgröße (full HD)</li>" +
		"<li>\n&#8226;	Mikrofon</li>" +
		"<li>\n&#8226;	Präsentationsprogramm</li>" +
		"<li>\n&#8226;	Screenrecordingprogramm" +
		"<li>\n&#8226;	Kompressionsprogramm</li></ul>" +
		"\n Verwenden Sie zugängliche und/oder weitverbreitete Programme, damit Ihr Video von jedem und jeder ohne Kosten verwendet, bearbeitet und angepasst werden kann",

		"dataformat": "Geeignete Formate für Videos sind:<br>\n" +
		"<ul><li>\n&#8226;	MP4/H.264 (p)</li>" +
		"<li>\n&#8226;	MP4/H.265 (p)</li>" +
		"<li>\n&#8226;	WEBM/VPx</li>" +
		"<li>\n&#8226;	AVIF/AV1</li></ul>"};

var b1_7_array = {"b1_1": "Skript",
		"heading": "Individueller Leitfaden zur Erstellung von Skripten",
		"intro": "Ein Skript dient sowohl als Orientierung während der Vorlesung als auch für die Vor- und Nachbereitung der Veranstaltung für Studierende. Bereiten Sie die Inhalte so auf, dass es den Wissenserwerb der Lernenden unterstützt\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Wählen Sie einen eindeutigen Titel (auch für die Auffindbarkeit im OER-Portal)." +
		"<li>\n&#8226;	Das Skript veranschaulicht eingangs die thematischen Schwerpunkte und stellt diese in einen Zusammenhang." +
		"<li>\n&#8226;	Typographische Elemente heben zentrale Aspekte hervor." +
		"<li>\n&#8226;	Die Textgestaltung und weitere Medienelemente (Kontrast, Farben, Schriftgröße) sind barrierearm gehalten.</ul>\n\n",

		"tool": "Verwenden Sie zugängliche und weitverbreitete Programme, damit Ihr Skript von jedem und jeder ohne Kosten verwendet, bearbeitet und angepasst werden kann. Geeignete Texteditoren für OER sind: Libre Office, MS Office, OnlyOffice, LaTeX  sowie Gitlab.<br>\n",

		"dataformat": "Speichern Sie das Skript in einem offenen Dateiformat und deaktivieren Sie ggf. den Schreibschutz. Geeignete Formate sind je nach Texteditor: docx, odt, tex und für webbasierte Darstellungen md, html und tex.<br>\n"};

var b1_8_array = {"b1_1": "Audiopodcast",
		"heading": "qqq",
		"intro": "intor",
		"dataformat": "Infografik prob1",
		"tool": "JJU"};

var b1_9_array = {"b1_1": "Abbildungen",
		"heading": "Individueller Leitfaden bei der Erstellung von Abbildungen",
		"intro": "Mithilfe von Abbildungen können Inhalte visuell dargestellt werden, um den Lernprozess zu unterstützen. Sobald ein Inhalt oder Gegenstand mehr oder weniger abstrakt visuell wiedergeben wird, handelt es sich um eine Visualisierung. Dieser Inhalt oder Gegenstand kann konkreter oder abstrakter Art sein. (z.B. reales Bild oder Prozessabbildung). Infografiken etwa visualisieren teils komplexe Sachverhalte prägnant und unterstützen den Lernprozess visuell. Dabei wird häufig ein Bild mit einem Text kombiniert. Infografiken basieren auf Daten und können Prozessen oder Theorien abbilden\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Der Titel der Abbildung ist eindeutig (auch für die Auffindbarkeit im OER-Portal)." +
		"<li>\n&#8226;	Die Abbildungen sind mit wesentlichen Beschreibungen versehen (Titel der Abbildung)." +
		"<li>\n&#8226;	Die Bildelemente haben eine gute Auflösung." +
		"<li>\n&#8226;	Die Abbildungen (Kontrast, Farben, Schriftgröße) sind barrierearm gestaltet.</ul>\n\n",

		"tool": "Besonders geeignet für die Erstellung von Abbildungen ist das browserbasierte Tool <a href=\"https://app.diagrams.net/\">draw.io</a>. Hier können Abbildungen entweder lokal oder in der Cloud gespeichert werden. Es stehen viele verschiedene Vorlagen (z. B. Diagramme, Prozessvisualisierung) zur Verfügung.<br>\n" +

		"<ul><li>\n&#8226;	<a href=\"https://app.diagrams.net/\">draw.io</a>",

		"dataformat": "Wollen Sie die Bilder zur Nutzung bereitstellen, dann speichern Sie die Abbildungen im png-oder jpeg-, tif-, tiff-/gif-,giff -Format ab, zur Ermöglichung von Weiterbearbeitung im xml-Datei. Für webbasierte Darstellungen verwenden Sie das svg-Formaty<br>\n"};


var b1_10_array = {"b1_1": "Präsentationsfolien",
		"heading": "Individueller Leitfaden zur Erstellung von Folien",
		"intro": "Präsentationsfolien dienen in erster Linie als Visualisierungsergänzung zu einem mündlichen Vortrag. Foliensätze werden auch als Skripte eingesetzt und Studierenden zur Verfügung gestellt. Effektiv sind Folien, wenn sie nicht zu viel Text enthalten. Empfohlen wird, dass jede Folie eine Botschaft enthalten sollte. Visualisierungen sollen vom Inhalt nicht ablenken, sondern diesen unterstützen\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Der Titel der Präsentation ist eindeutig (auch für die Auffindbarkeit im OER-Portal)." +
		"<li>\n&#8226;	Lehr- oder Lernziele und der Ablauf werden eingangs auf max. zwei Folien expliziert." +
		"<li>\n&#8226;	Eine einheitliche Gestaltung der Folien wirkt sturkturierend. (Angaben wie Name, Fakultät, Logo müssen nicht auf jeder Folie angegeben werden)." +
		"<li>\n&#8226;	Seitenzahlen (Angabe wie etwa 1/10) oder Navigationsleiste geben eine Orientierung." +
		"<li>\n&#8226;	Textelemente sollten kurzgehalten und in Textblöcken formatiert werden." +
		"<li>\n&#8226;	Sanserife Schriftarten und Schriftgröße 24 pt eignen sich zur Foliengestaltung." +
		"<li>\n&#8226;	Eine Lessons Learned-Folie am Ende fasst wesentliche Inhalte zusammen." +
		"<li>\n&#8226;	Folien und ggf. weitere Medienelemente sind barrierearm gehalten (Kontrast, Farben, Schriftgröße).</ul>\n\n",

		"tool": "Geeignete Programme zur Erstellung für OER sind: LibreOffice, OpenOffice, MS Office PowerPoint, OnlyOffice und LaTeX.<br>\n" ,

		"dataformat": " Speichern Sie den Foliensatz in einem offenen Dateiformat und deaktivieren Sie den Schreibschutz. Geeignete Formate sind je nach Anwendungssoftware: pptx pps, ppsx, odp.<br>\n"};



var b1_11_array = {"b1_1": "Infografik",
		"heading": "Individueller Leitfaden zur Erstellung von Infografiken",
		"intro": "Infografiken visualisieren teils komplexe Sachverhalte prägnant und unterstützen den Lernprozess visuell. Dabei wird häufig ein Bild mit einem Text kombiniert. Infografiken basieren auf Daten und können Prozessen oder Theorien abbilden\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Der Titel der Infografik ist eindeutig (auch für die Auffindbarkeit im OER-Portal)." +
		"<li>\n&#8226;	Die Abbildungen haben eine gute Auflösung." +
		"<li>\n&#8226;	Die Infografik enthält Kernaussagen, die durch möglichst einfache Darstellungen/Datendarstellungen präsentiert werden." +
		"<li>\n&#8226;	Die Textelemente sollten in Textblöcken formatiert werden." +
		"<li>\n&#8226;	Abbildungen sind barrierearm gehalten (Kontrast, Farben, Schriftgröße).</ul>\n\n",

		"tool": "Besonders geeignet für die Erstellung von Infografiken ist das browserbasierte Tool <a href=\"https://app.diagrams.net/\">draw.io.</a>. Hier können Abbildungen entweder lokal oder in der Cloud gespeichert werden. Das Importieren von Dateien wird in der Anwendung ermöglicht. Es stehen viele verschiedene Templates (z. B. Diagramme, Prozessvisualisierung) zur Verfügung.<br>\n",

		"dataformat": "Wollen Sie die Infografik zur Nutzung bereitstellen, dann speichern Sie die Abbildungen im png-oder jpeg-, tif-, tiff-/gif-,giff -Format ab, zur Ermöglichung von Weiterbearbeitung speichern Sie die Abbildungen im xml-Datei ab. Für webbasierte Darstellungen verwenden Sie das svg-Format.<br>\n"};


var b1_12_array = {"b1_1": "Poster",
		"heading": " Individueller Leitfaden zur Erstellung von Postern",
		"intro": "Wissenschaftliche Poster werden im akademischen Bereich häufig als Präsentationsmedium eingesetzt. Dabei werden Forschungsvorhaben oder Projektverläufe dargestellt, indem Bild und Text kombiniert werden\n"+
		"<br><br>\n<h4><strong>Gestaltungshinweise</strong></h4>\n<ul>" +
		"<li>\n&#8226;	Der Titel des Posters ist eindeutig (auch für die Auffindbarkeit im OER-Portal)." +
		"<li>\n&#8226;	Die Über- und Zwischenüberschriften für die Posterelemente strukturieren den Inhalt." +
		"<li>\n&#8226;	Die Anordnung der Posterlemente bilden gleichsam eine Gliederung ab." +
		"<li>\n&#8226;	Abbildungen sind barrierearm gehalten (Kontrast, Farben, Schriftgröße) und weisen eine gute Auflösung auf." +
		"<li>\n&#8226;	Textelemente sollten in Textblöcken formatiert werden und haben einen ausreichenden Abstand zu den Posterelementen." +
		"<li>\n&#8226;	Auf eine angemessene Schriftgröße sollte geachtet werden: Überschrift 40pt, Fließtext 24pt, Bildbeschriftungen 20pt.</ul>\n\n",

		"tool": "\n<h4><strong>Werkzeug</strong></h4>\n<ul>" +
				"Besonders geeignet für die Erstellung ist das browserbasierte Tool <a href=\"https://app.diagrams.net/\">draw.io</a>. Hier können Abbildungen entweder lokal oder in der Cloud gespeichert werden. Das Importieren von Dateien wird in der Anwendung ermöglicht. Es stehen viele verschiedene Templates (z. B. Diagramme, Prozessvisualisierung) zur Verfügung. Weitere Editoren sind: PowerPoint, Onlyoffice, LibreOffice, OpenOffice.<br>\n",

		"dataformat": "Wollen Sie das Poster zur Nutzung bereitstellen, dann speichern Sie es im PDF -Format ab, zur Ermöglichung von Weiterbearbeitungen speichern Sie die Datei je nach gewählten Werkzeug als xml-Datei oder pptx, odp, odg ab.<br>\n"};


var b1_13_array = {"b1_1": "Advance Organizer",
		"heading": "fssrrrs",
		"intro": "intor",
		"dataformat": "Infografik prob1",
		"tool": "JJU"};

var b1_14_array = {"b1_1": "Audiopodcast",
		"heading": "fvvsss",
		"intro": "intor",
		"dataformat": "Infografik prob1",
		"tool": "JJU"};
