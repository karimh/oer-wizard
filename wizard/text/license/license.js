/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var license_1 = {"l1": "CC-0"}
var license_2 = {"l1": "CC-BY"}
var license_3 = {"l1": "CC-BY-SA"}
var license_4 = {"l1": "CC-BY-ND"}
var license_5 = {"l1": "CC-BY-NC"}
var license_6 = {"l1": "CC-BY-NC-SA"}
var license_7 = {"l1": "CC-BY-NC-ND"}
var license_8 = {"l1": "CC-PD-MARK"}
var license_9 = {"l1": "Urheberrechtlich geschützt"}




var license_10 = {"l1": "CC-0"}
var license_11 = {"l1": "CC-BY 4.0"}
var license_12 = {"l1": "CC-BY-SA 4.0"}
var license_13 = {"l1": "CC-BY-ND 4.0"}
var license_14 = {"l1": "CC-BY-NC 4.0"}
var license_15 = {"l1": "CC-BY-NC-SA 4.0"}
var license_16 = {"l1": "CC-BY-NC-ND 4.0"}



//copyright
var copyright_1 = "Ihr Material wird ausschließlich aus eigenen Inhalten bestehen. Daher können Sie diese nach Ihren eigenen Bedürfnissen lizenzieren und müssen keine Nutzungsbedingungen vorab klären. Im Sinne der OER Praxis empfehlen wir, eine möglichst offene Lizenz zu wählen (public domain, CC 0, CC BY und CC BY SA). Neben der maschinellen Auslesbarkeit der Lizenzen, die Sie im OER-Portal im Einstellungsprozess realisieren, sollten Sie die Lizenzhinweise bereits bei der Erstellung deutlich sichtbar auf Ihrem Bildungsmaterial angeben. Wichtige Hinweise für den Umgang mit Lizenztexten und -angaben erhalten Sie unter Schritt 4: Lizenzen transparent machen.";

var copyright_2 = "Sie planen Materialien oder Inhalte Dritter in Ihrem Bildungsmaterial einzubinden. Deshalb prüfen Sie in einem ersten Schritt, ob Sie berechtigt sind, das von Ihnen ausgewählten Material zu nutzen. Achten Sie darauf, welche Lizenzangaben die urhebende Person des ausgewählten Werks macht. Beachten Sie folgende allgemeine Hinweise. Detaillierte Informationen zum Umgang mit Lizenztexten und -angaben erhalten Sie unter Schritt 4: Lizenzen transparent machen.\n" +
					"</br></br><strong>Hinweise für die Wiederveröffentlichung von fremden Materialien:</strong></b></br>" +
					"Geben Sie die Lizenzangaben der urhebenden Personen in unmittelbarer Nähe zum Werk an." +
					"Beim Vermischen von mehreren Materialien mit unterschiedlichen Lizenzen müssen Sie bei der Wiederveröffentlichung Ihres Werkes darauf achten, sich an dem Werk mit der höchsten Lizenzeinschränkung zu orientieren." +
					"Bei Unsicherheiten können Sie den <a href=\"http://ccmixer.edu-sharing.org/\" target=\"_blank\" rel=\"noopener noreferrer\">CC Mixer</a> heranziehen, um die entsprechende Lizenz zu ermitteln.";

var copyright_3 = "Prüfen Sie zunächst an Hand der Lizenztexte, ob Sie berechtigt sind, die fremden Materialien oder Inhalte zu verändern bzw. weiterzuentwickeln. Geben Sie die von der urhebenden Person gemachten Angaben korrekt an.</br></br>\n" +
					"<ol><li>	Geben Sie die Lizenzangaben der urhebenden Personen in unmittelbarer Nähe zum Werk an.</li>"+
					"<li>	Beim Vermischen von mehreren Ressourcen mit unterschiedlichen Lizenzen müssen Sie bei der Wiederveröffentlichung Ihres Werkes (Sammelwerk) darauf achten, sich an dem Werk mit der größten Lizenzeinschränkung zu orientieren und entsprechend zu veröffentlichen.</li></ol>"+
						"<strong>Hinweise für die Wiederveröffentlichung von fremden Materialien:</strong></b></br>" +
						"</br>Steht das von Ihnen verwendete fremde Werk nicht allein, sondern wird von Ihnen sichtbar gerahmt, dann können Sie mithilfe eines Textbausteins auf die Eigenständigkeit der Werke hinweisen.  z.B. Dieses Werk und dessen Inhalte sind - sofern nicht anders angegeben-  lizenziert unter (um Lizenzangaben ergänzen). Dies gilt aber nicht für Materialien, die urheberrechtlich geschützt sind oder ein Copyright aufweisen. Positionieren Sie diesen Textbaustein gut sichtbar auf Ihrem Werk!" +
						"Die Lizenzwahl richtet sich andernfalls am verwendeten CC-lizenzierten Werk mit der höchsten Lizenzeinschränkung. Bei Unsicherheiten können Sie den <a href=\"http://ccmixer.edu-sharing.org/\" target=\"_blank\" rel=\"noopener noreferrer\">CC Mixer</a> heranziehen, um die entsprechende Lizenz anzugeben.";

var copyright_4 = "Sie planen Materialien oder Inhalte Dritter für den eigenen Lehrkontext anzupassen. Deshalb prüfen Sie in einem ersten Schritt, ob Sie berechtigt sind, das von Ihnen ausgewählte Material zu nutzen und zu bearbeiten. Schauen Sie, unter welcher Lizenz das ausgewählte Werk steht. Werke die unter einer CC 0 oder CC BY Lizenz stehen, dürfen verändert oder angepasst werden. Die Lizenzangaben der urhebenden Person, müssen in Ihrem Lizenzhinweis aufgenommen werden. Beachten Sie folgende allgemeine Hinweise. Detaillierte Informationen zum Umgang mit Lizenztexten und -angaben erhalten Sie unter Schritt 4: Lizenzen transparent machen." +
					"<ol><li>Wenn Sie mehrere Bildungsmaterialien mit unterschiedlichen Lizenzen kombinieren möchten, müssen Sie bei der Wiederveröffentlichung Ihres Werks darauf achten, sich an dem Werk mit der höchsten Lizenzeinschränkung zu orientieren.</li>"+
					"<li>Ergänzen Sie diese mit der von Ihnen vorgenommenen Änderung des Originals (Art der Veränderung z.B. Ergänzung, Designanpassung).</li>"+
					"<li>Je nach Lizenztext ergänzen Sie Ihren Namen (Vor- und Nachname bzw. Institution oder Pseudonym).</li>"+
					"<li>Geben Sie entweder den übernommenen bzw. rechtmäßig veränderten Lizenztext an.</li></ol>"+
						"<strong>Hinweise für die Wiederveröffentlichung von fremden Materialien:</strong></br>" +
						"</br>" +
						"Bei Unsicherheiten können Sie den <a href=\"http://ccmixer.edu-sharing.org/\" target=\"_blank\" rel=\"noopener noreferrer\">CC Mixer</a> heranziehen, um die entsprechende Lizenz zu ermitteln.";



//license
var cc0_a = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC 0 stehen. Sie dürfen das Material uneingeschränkt nutzen und wiederveröffentlichen. Geben Sie lediglich so nah wie möglich am entsprechenden Werk oder Inhalt die Lizenzangabe CC-0 an, damit erkennbar bleibt, dass es sich um ein gemeinfreies Werk handelt.</br></br>" +
					"Vorgenommene Änderungen am Original können ggf. im Lizenztext durch Angaben der Art und Weise und Ihren Namen ergänzt werden." +
					"</br></br>Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen::</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) und ggf. Titel können optional angegeben werden,</li>" +
					"<li>Lizenztext CC-0 mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC BY stehen. Sie dürfen das Material uneingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vor- und Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben.</br></br>" +
					"Die Angaben zu den fremden Inhalten bzw. Materialien sind verpflichtend mitzuführen:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) kann optional angegeben werden,</li>" +
					"<li>Titel des Werkes (bei Lizenzversion 3.0 DE verpflichtend bei 4.0 International optional),</li>" +
					"<li>Vor- und Nachname der urhebenden Person bzw. angegebener Name,</li>" +
					"<li>Lizenztext CC-BY mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen,</li>" +
					"<li>Vorgenommene Änderungen am Original sollten in der Lizenzangabe durch Angaben der Art und Weise der Änderung und Ihren Namen ergänzt werden.</li></ol>";

var cc_by_sa = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC BY SA. Sie dürfen das Material uneingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vor- und Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben.</br></br>" +
					"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) kann optional angegeben werden,</li>" +
					"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
					"<li>Vor- und Nachname der urhebenden Person bzw. angegebene Name,</li>" +
					"<li>Lizenztext CC-BY-SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen.</li>" +
					"<li>vorgenommene Änderungen am Original sollten in der Lizenzangabe durch Angaben der Art und Weise der Änderung und Ihren Namen ergänzt werden.</li></ol>";

var cc_by_nc = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC BY NC stehen zu verwenden. Bitte beachten Sie: Sie dürfen das Material eingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vor- und Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und es nicht für kommerzielle Zwecke nutzen (Entgelt für die Nutzung verlangen u.a. Workshopgebühren).</br></br>" +
					"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) kann optional angegeben werden,</li>" +
					"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
					"<li>Vor- und Nachname der urhebenden Person bzw. angegebener Name,</li>" +
					"<li>Lizenztext CC-BY-NC mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen,</li>" +
					"<li>vorgenommene Änderungen am Original sollten in der Lizenzangabe durch Angaben der Art und Weise der Änderung und Ihren Namen ergänzt werden.</li></ol>";

var cc_by_nd = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC BY ND. Bitte beachten Sie: Sie dürfen das Material nur eingeschränkt nutzen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben. Eine Bearbeitung ist nicht erlaubt.</br></br>" +
					"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) kann optional angegeben werden,</li>" +
					"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
					"<li>Vor- und Nachname der urhebenden Person bzw. angegebener Name,</li>" +
					"<li>Lizenztext CC-BY-NC mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var cc_by_nc_sa = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC BY NC SA. Bitte beachten Sie: Sie dürfen das Material nur eingeschränkt nutzen und wiederveröffentlichen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und es nicht für kommerzielle Zwecke nutzen (Entgelt für die Nutzung verlangen u.a. Workshopgebühren) und solange das neu entstandene Werk unter den gleichen Bedingungen weitergegeben und veröffentlicht wird.</br></br>" +
					"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) kann optional angegeben werden,</li>" +
					"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
					"<li>Vor- und Nachname der urhebenden Person bzw. angegebener Name,</li>" +
					"<li>Lizenztext CC-BY-NC-SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen.</li>" +
					"<li>Vorgenommene Änderungen am Original sollten in der Lizenzangabe durch Angaben der Art und Weise der Änderung und Ihren Namen ergänzt werden.</li></ol>";

var cc_by_nc_nd = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die unter der Lizenz CC BY NC ND stehen. Bitte beachten Sie: Sie dürfen das Material nur eingeschränkt nutzen, wenn Sie den Namen (Vorname Nachname) der urhebenden Person in unmittelbarer Nähe zum Werk (Inhalt) angeben und es nicht für kommerzielle Zwecke nutzen (Entgelt für die Nutzung verlangen u.a. Workshopgebühren) und keine Bearbeitungen vornehmen.</br></br>" +
					"Die Angaben zu den fremden Inhalten oder Materialien sind verpflichtend mitzuführen:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video etc.) kann optional angegeben werden,</li>" +
					"<li>Titel des Werkes (bei Lizenzversion 3.0 Deutschland verpflichtend, bei 4.0 International optional),</li>" +
					"<li>Vor- und Nachname der urhebenden Person bzw. angegebener Name,</li>" +
					"<li>Lizenztext CC-BY-NC-ND mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";

var copy1 = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die urheberrechtlich geschützt sind. Bitte beachten Sie: Materialien oder Inhalte, die urheberrechtlich geschützt sind, dürfen weder kopiert, vermischt, bearbeitet, noch von Ihnen (wieder-)veröffentlicht werden. Deshalb dürfen Sie diese Materialien grundsätzlich nicht verwenden. Inhalte Dritter dürfen im Rahmen der gesetzlichen Schranken des Urheberrechts rechtskonform verwendet werden, insbesondere die redliche Verwendung im Rahmen der wissenschaftlichen Praxis, z.B. die Zitatfreiheit nach § 51 UrhG. Wir empfehlen Ihnen im OER-Portal oder anderen Repositorien nach alternativen Materialien zu suchen, die eine ausdrückliche Nachnutzung erlauben.</br></br>" +
					"Wir empfehlen im OER Portal nach alternativen Bildungsressourcen zu suchen, die eine ausdrückliche Nachnutzung erlauben.";

var cc_pd_mark = "Sie geben in Ihrer Planung an, fremde Materialien zu verwenden, die gemeinfrei sind. Sie dürfen Inhalte oder Materialien, die gemeinfrei sind, uneingeschränkt nutzen und wiederveröffentlichen. Geben Sie lediglich, so nah wie möglich am entsprechenden Werk oder Inhalt die Lizenzangabe Public Domain Mark (CC-PD-MARK) an, damit erkennbar bleibt, dass es sich um ein gemeinfreies Werk handelt. Deshalb gilt a) Gemeinfreie Werke dürfen unter Angabe der Art der Änderung verändert werden und </br></br>" +
					"Die Angaben des verwendeten Werks sollten mitgeführt werden:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild; Text; Video etc.) und ggf. Titel können optional angegeben werden,</li>" +
					"<li>Lizenztext Public Domain Mark mit Verlinkung auf Creative Commons Homepage + Lizenzversion (Achten Sie auf die Lizenzversion),</li>" +
					"<li>ggf. Hyperlink zur Quelle anfügen.</li></ol>";






var cc0_b = "Sie geben in Ihrer Planung an, Ihr Material uneingeschränkt zur freien Nutzung zur Verfügung zu stellen. Mit dem Lizenztext CC-0 erlauben Sie anderen Ihr Werk unbeschränkt und ohne Bedingungen zu verwenden. Es darf also kopiert, veröffentlicht, weiterbearbeitet und verändert wieder bereitgestellt werden. Aus Wertschätzung wird in der Regel trotzdem der Urheber*die Urheberin genannt. Kopien und bearbeitete Versionen können auch kommerziell genutzt werden.</br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Ihren eigenen Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://oerhoernchen.de/bildungsteiler/\" target=\"_blank\" rel=\"noopener noreferrer\">Bildungsteiler</a> vom OERhörnchen generieren lassen. Dort geben Sie die angeforderten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen können.</br>" +
					"</br></br>So beschriften Sie Ihr Bildungsmaterial eindeutig:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</br>" +
					"<li>Lizenztext CC 0 mit Verlinkung auf Creative Commons Homepage + Lizenzversion,</br>" +
					"<li>ggf. Lizenzicon mit der Attribution Zero einfügen.</li></ol>";

var cc_by_4 = "<strong>Sie geben in Ihrer Planung an, Ihr Material unter der Bedingung der Namensnennung, uneingeschränkt zur freien Nutzung zur Verfügung zu stellen.</strong> Mit dem Lizenztext CC-BY erlauben Sie anderen Ihr Werk unbeschränkt unter der Bedingung, dass Ihr angegebener Name korrekt aufgeführt wird, zu verwenden. Es darf also kopiert, weiterbearbeitet, verändert oder unverändert veröffentlicht werden. Sie erlauben auch Kopien und bearbeitete Versionen kommerziell zu nutzen.</br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Ihren eigenen Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://oerhoernchen.de/bildungsteiler/\" target=\"_blank\" rel=\"noopener noreferrer\">Bildungsteiler</a> vom OERhörnchen generieren lassen. Dort geben Sie die angeforderten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen können.</br>" +
					"</br></br>So beschriften Sie Ihr Bildungsmaterial eindeutig:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
					"<li>Vor- und Nachname ggf. Institution bzw. Pseudonym,</br>" +
					"<li>Lizenztext CC BY mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
					"<li>ggf. Lizenzicon mit der Attribution Namensnennung einfügen.</li></ol>";

var cc_by_sa_4 = "<strong>Sie geben in Ihrer Planung an, Ihr Material unter der Bedingung der Namensnennung zur freien Nutzung zur Verfügung zu stellen und möchten, dass Materialien, die auf der Grundlage Ihres Werks basieren, unter den gleichen Bedingungen (wieder-)veröffentlicht werden.</strong> Mit dem Lizenztext CC-BY-SA erlauben Sie anderen Ihr Werk unbeschränkt unter der Bedingung, dass Ihr angegebener Name korrekt aufgeführt wird und alle bearbeiteten Versionen unter derselben Lizenz veröffentlicht werden, zu verwenden.</br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Ihren eigenen Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://oerhoernchen.de/bildungsteiler/\" target=\"_blank\" rel=\"noopener noreferrer\">Bildungsteiler</a> vom OERhörnchen generieren lassen. Dort geben Sie die angeforderten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen können.</br>" +
					"</br></br>So beschriften Sie Ihr Bildungsmaterial eindeutig:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
					"<li>Vor- und Nachname ggf. Institution bzw. Pseudonym,</br>" +
					"<li>Lizenztext CC BY SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
					"<li>ggf. Lizenzicon mit den Attributationen Namensnennung - Weitergabe unter gleichen Bedingungen einfügen.</li></ol>";

var cc_by_nc_4 = "<strong>Sie geben in Ihrer Planung an, Ihr Material unter der Bedingung der Namensnennung und zur Nutzung für ausschließlich nicht kommerzielle Zwecke zur Verfügung zu stellen.</strong>, Die Einschränkung NC bringt einige Nachteile für andere bei der Nachnutzung. In der Praxis könnte das z.B. bedeuten, dass Ihre Bildungsressource im Rahmen eines Workshops im Hochschulkontext von Dritten nicht zum Einsatz kommen darf. </br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Den Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://creativecommons.org/choose/?lang=de/\" target=\"_blank\" rel=\"noopener noreferrer\">License Chooser</a> von Creative Commons generieren lassen. Dort geben Sie die wichtigsten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen.</br>" +
					"</br></br>So lizenzieren Sie Ihr Bildungsmaterial eindeutig:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
					"<li>Vor- und Nachname ggf. Institution bzw. Pseudonym,</li>" +
					"<li>Lizenztext CC BY NC mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
					"<li>ggf. Lizenzicon mit den Attributationen Namensnennung - Nicht kommerziell einfügen.</li></ol>";

var cc_by_nc_sa_4 = "<strong>Sie geben in Ihrer Planung an, Ihr Material unter der Bedingung der Namensnennung und zur Nutzung für ausschließlich nicht kommerzielle Zwecke zur Verfügung zu stellen. Materialien, die auf der Grundlage Ihres Werks basieren, sollen unter den gleichen Bedingungen veröffentlicht werden.</strong> Die Einschränkung NC bringt einige Nachteile für andere bei der Nachnutzung. In der Praxis könnte das z.B. bedeuten, dass Ihre Bildungsressource z. B. im Rahmen eines Workshops im Hochschulkontext von Dritten nicht verwendet werden darf.</br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Den Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://creativecommons.org/choose/?lang=de/\" target=\"_blank\" rel=\"noopener noreferrer\">License Chooser</a> von Creative Commons generieren lassen. Dort geben Sie die wichtigsten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen.</br>" +
					"</br></br>So beschriften Sie Ihr Bildungsmaterial eindeutig:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
					"<li>Vor- und Nachname ggf. Institution bzw. Pseudonym,</li>" +
					"<li>Lizenztext CC BY NC SA mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
					"<li>ggf. Lizenzicon mit den Attributionen Namensnennung - Nicht kommerziell - Weitergabe unter gleichen Bedingungen einfügen.</li></ol>";

var cc_by_nd_4 = "<strong>Sie geben in Ihrer Planung an, Ihr Material unter der Bedingung der Namensnennung zur Nutzung zur Verfügung zu stellen und möchten keine Bearbeitungen oder Anpassungen zulassen.</strong> Dann verwenden Sie den Lizenztext CC-BY-ND. Diese Lizenzwahl bringt einige Nachteile für andere bei der Nachnutzung, da die Möglichkeit einer Anpassung an den eigenen Lehrkontext nicht gegeben ist.</br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Den Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://creativecommons.org/choose/?lang=de/\" target=\"_blank\" rel=\"noopener noreferrer\">License Chooser</a> von Creative Commons generieren lassen. Dort geben Sie die wichtigsten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen.</br>" +
					"</br></br>So beschriften Sie Ihr Bildungsmaterial eindeutig:</br></br>" +
					"<ol><li>Werkbezeichnung (z. B. Bild, Text, Video, etc.) und Titel können optional angegeben werden,</li>" +
					"<li>Vor- und Nachname ggf. Institution bzw. Pseudonym,</li>" +
					"<li>Lizenztext CC BY ND mit Verlinkung auf Creative Commons Homepage + Lizenzversion (wir empfehlen 4.0),</li>"+
					"<li>ggf. Lizenzicon mit den Attributionen Namensnennung - keine Bearbeitung.</li></ol>";

var cc_by_nc_nd_4 = "<strong>Sie geben in Ihrer Planung an, Ihr Material unter der Bedingung der Namensnennung und zur Nutzung für ausschließlich nicht kommerzielle Zwecke zur Verfügung zu stellen. Zudem möchten Sie Bearbeitungen oder Anpassung Ihres Werks nicht zulassen.</strong> Die Einschränkungen NC und ND bringen einige Nachteile für andere bei der Nachnutzung. Denn eine Bearbeitung und Anpassung an den eigenen Kontext wird verweigert und die Nutzung könnte in der Praxis bedeuten, dass Ihr Material z. B im Rahmen eines Workshops im Hochschulkontext von Dritten nicht verwendet werden darf.</br></br>" +
					"Bei Abbildungen kann der Lizenztext unterhalb des Materials positioniert werden. Bei Texten oder Folien ist es üblich diesen auf der ersten oder letzten Seite bzw. Folie zu setzen. Im Video kann der Lizenztext im Vor- oder Abspann angegeben werden.</br>" +
					"Den Lizenztext können Sie mithilfe der browserbasierten Anwendung <a href=\"https://creativecommons.org/choose/?lang=de/\" target=\"_blank\" rel=\"noopener noreferrer\">License Chooser</a> von Creative Commons generieren lassen. Dort geben Sie die wichtigsten Lizenzinformationen ein und erhalten Ihren Lizenztext, den Sie einfach kopieren und auf Ihrem Bildungsmaterial einfügen.</br>";