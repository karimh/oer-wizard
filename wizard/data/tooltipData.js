/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-11
 * @modify date 2020-11-11
 * @desc [description]
 */
 var warning = "<div class='atention'>Das OER-Planungstool befindet sich aktuell im Aufbau. Leider werden noch keine Hinweise zu Lektion und Kurs angezeigt.";

var TOOLTIP_DATA=[
        {
            //Inhaltstype
            id:"step1-b1",
            title:"<div class='tooltip1'>Bildungsmaterialien, die einen fachlichen Informationsgehalt haben und zur Veranschaulichung von Lehrinhalten dienen"
        },
        {
            id:"step1-b2",
            title:"<div class='tooltip1'>Bildungsmaterialien mit Aufforderungscharakter an Lernende eine Lernhandlung durchzuführen"
        },
        {
            id:"step1-b3",
            title: "<div class='tooltip1'>Eine Lektion umfasst mehrere Informationsmaterialien und Aufgaben/Übungen. Zum Beispiel eine Moolde-Lektion oder ein H5P-Szenario, das mehrere Dokumente, Lernaufgaben und Interaktionen beinhaltet bzw. Skripte aus denen diese Elemente klar hervorgehen."
        },
        {
            id:"step1-b4",
            title: "<div class='tooltip1'>Ein Kurs ist eine in sich geschlossene Lehrveranstaltung, die aus mehreren Einheiten (Lektionen) besteht. Ein Kurs beinhaltet mehrere Informationsmaterialien, Lernaufgaben und Interaktionen. Ggf. Skripte aus denen eine Kursstruktur klar hervorgeht"
        },
        {
            //Kleinteiligs Lehrmaterial
            id:"b1_1",
            title: "<div class='tooltip1'>Ausgewählte und strukturierte Lehrinhalte im Textformat"
        },
        {
            id:"b1_2",
            title: "<div class='tooltip1'>Eine im Hörsaal aufgezeichnete, vollständige Lehrveranstaltung oder ein aufgezeichneter, vollständiger Vortrag"
        },
        {
            id:"b1_3",
            title: "<div class='tooltip1'>Eine mit wenig Aufwand produzierte Aufnahme eines Vortragenden"
        },
        {
            id:"b1_4",
            title: "<div class='tooltip1'>Eine mit Audiokommentaren unterlegte Aufzeichnung von Präsentationsfolien"
        },
        {
            id:"b1_5",
            title: "<div class='tooltip1'>Eine mit Audiokommentaren unterlegte Aufzeichnung von Bildschirmaktivitäten"
        },
        {
            id:"b1_6",
            title: "<div class='tooltip1'>Eine mit Audiokommentaren aufgezeichnete Aufnahme, auf der ausgeschnittene Figuren ein- und ausgeblendet werden"
        },
        {
            id:"b1_7",
            title: "<div class='tooltip1'>Schriftliche Dokumentation der vorzutragenden Lehrinhalte, die auf eine inhaltliche, visuelle und didaktische Anschaulichkeit der Inhalte zielt"
        },
        {
            id:"b1_9",
            title: "<div class='tooltip1'>Abbildungen sind Visualisierungsformen wie Fotografien, logische Bilder (Diagramme, Charts, Tabellen) oder abstrahierte Darstellungen (Symbole und Metaphern)"
        },
        {
            id:"b1_10",
            title: "<div class='tooltip1'>Begleitmaterial/ Visualsierungsmedium für einen mündlichen Vortrag/Vorlesung"
        },
        {
            id:"b1_11",
            title: "<div class='tooltip1'>Darstellungen von komplexen Sachverhalten (Infografiken können auf Daten basieren oder Prozesse und Theorien schematisch abbilden)"
        },
        {
            id:"b1_12",
            title: "<div class='tooltip1'>Visuelles Mittel zur Darstellung von Informationen"
        },
        {
            //Aufgabe
            id:"b2_1",
            title: "<div class='tooltip1'>Skripte mit integrierten Fragen und Aufgaben zu den präsentierten Inhalten"
        },
        {
            id:"b2_2",
            title: "<div class='tooltip1'>Folien mit integrierten Aufgaben/Übungen zu den präsentierten Inhalten"
        },
        {
            id:"b2_3",
            title: "<div class='tooltip1'>Lehrtexte mit integrierten Aufgaben/Übungen zu den präsentierten Inhalten"
        },
        {
            id:"b2_4",
            title: "<div class='tooltip1'>Im Internet oder Intranet zur Verfügung gestellte Problemstellung, die vorrangig mit Quellen aus dem Internet bearbeitet wird"
        },
        {
            id:"b2_5",
            title: "<div class='tooltip1'>Individuelle Lernmappe zur Dokumentation von Lernprozessen und Lernergebnissen"
        },
        {
            id:"b2_6",
            title: "<div class='tooltip1'>Fragespiel mit Lern- bzw. Übungscharakter"
        },
        {
            id:"b2_7",
            title: "<div class='tooltip1'>Simulation von Entscheidungsprozessen in einem konstruierten Rahmen mit vorgegebenen Regeln"
        },
        {
            id:"b2_8",
            title: "<div class='tooltip1'>Didaktisch strukturierter Arbeitsauftrag"
        },
        {

            //license
            id:"l1",
            title: "<div class='tooltip1'>Es werden alle Nutzungsrechte eingeräumt."
        },
        {
            id:"l2",
            title: "<div class='tooltip1'>Es werden alle Nutzungsrechte unter der Bedingung der Namensnennung des Urhebers eingeräumt."
        },
        {
            id:"l3",
            title: "<div class='tooltip1'>Es werden alle Nutzungsrechte unter Bedingungen der Namensnennung und der Weitrgabe unter gleichen Bedingungen eingeräumt"
        },
        {
            id:"l4",
            title: "<div class='tooltip1'>Eingeschränkte Nutzungsrechte liegen vor. Es darf nicht bearbeitet werden und die Namensnennung ist verpflichtend."
        },
        {
            id:"l5",
            title: "<div class='tooltip1'>Eingeschränkte Nutzungsrechte werden eingeräumt. Es darf nicht kommerziell genutzt werden und die Namensnennung ist verpflichtend."
        },
        {
            id:"l6",
            title: "<div class='tooltip1'>Eingeschränkte Nutzungsrechte. Es darf nicht kommerziell genutzt werden, es muss eine Namensnennung sowie die Weitergabe unter gleichen Bedingungen erfolgen."
        },
        {
            id:"l7",
            title: "<div class='tooltip1'>Stark eingeschränkte Nutzungsrechte. Die Namensnennungs ist verpflichtend. Das Werk darf nicht kommerziell genutzt und nicht bearbeitet werden."
        },
        {
            id:"l8",
            title: "<div class='tooltip1'>Unter Public Domain Mark sind gemeinfreie Werke gefasst. Es werden alle Nutzungsrechte eingeräumt. Prüfen Sie genau, ob es sich bei den fremden Materialien oder Inhalten um gemeinfreie Werke handelt."
        },
        {
            id:"l9",
            title: "<div class='tooltip1'>Bei urheberrechtlich geschützten Materialien (ohne weitere Lizenz oder Vermerk) sind alle Nutzungsrechte vorbehalten."
        },
        {
            //license 4.0
            id:"l10",
            title: "<div class='tooltip1'>Sie erlauben eine uneingeschränkte Nutzung. Diese Lizenz ist im OER Portal ausdrücklich erwünscht."
        },
        {
            id:"l11",
            title: "<div class='tooltip1'>Sie erlauben eine uneingeschränkte Nutzung unter der Bedingung der Namensnennung. Diese Lizenz ist im OER Portal ausdrücklich erwünscht."
        },
        {
            id:"l12",
            title: "<div class='tooltip1'>Sie erlauben eine uneingeschränkte Nutzung unter der Bedingung der Namensnennung und Weitergabe unter gleichen Bedingungen. Diese Lizenz ist im OER Portal ausdrücklich erwünscht."
        },
        {
            id:"l13",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung. Ihr Material darf zwar unter der Bedingung der Namensnennung geteilt, aber nicht bearbeitet werden."
        },
        {
            id:"l14",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung unter der Bedingung der Namensnennung und nicht komerzieller Nutzung."
        },
        {
            id:"l15",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung unter der Bedingung der Namensnennung, Nicht kommerziellen Nutzung und Weitergabe unter gleichen Bedingungen. Mit dieser Lizenz könnten Nachteile für die Community entstehen."
        },
        {
            id:"l16",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung. Ihr Material darf zwar unter der Bedingung der Namensnennung geteilt, aber nicht bearbeitet und kommerziell genutzt werden. Mit dieser Lizenz könnten Nachteile für die Community entstehen."
        },
        {
            //Lektion
            id:"b3_1",
            title: warning
        },
        {
            id:"b3_2",
            title: warning
        },
        {
            id:"b3_3",
            title: warning
        },
        {
            id:"b3_4",
            title: warning
        },
        {
            id:"b3_5",
            title: warning
        },
        {
            id:"b3_6",
            title: warning
        },
        {
            id:"b3_7",
            title: warning
        },
        {
            id:"b3_8",
            title: warning
        },
        {
            id:"b3_9",
            title: warning
        },
        {
            id:"b3_10",
            title: warning
        },
        {
            id:"b3_11",
            title: warning
        },
        {
            id:"b3_12",
            title: warning
        },
        {
            //Kurse
            id:"b4_1",
            title: warning
        },
        {
            id:"b4_2",
            title: warning
        },
        {
            id:"b4_3",
            title: warning
        },
        {
            id:"b4_4",
            title: warning
        }
    ]

