/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
$(document).ready(function() {
			
	$("#exampleModal").modal({
		backdrop: 'static',
		keyboard: false,
		show: true
	})

	infoClick();

	tooltip();

	$("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
		if (stepNumber != 0) {
			document.getElementById("reset").style.display = "block";
		}
		if (stepNumber == 0) {
			document.getElementById("reset").style.display = "none";
		}

	});

	$('#smartwizard').smartWizard({
		selected: 0,
		theme: 'arrows',
		transitionEffect: 'fade',
		showStepURLhash: false,
		toolbarSettings: {
			toolbarPosition: 'bottom', // none, top, bottom, both
			toolbarButtonPosition: 'left', // left, right, center
			showNextButton: false, // show/hide a Next button
			showPreviousButton: false, // show/hide a Previous button
			toolbarExtraButtons: [] // Extra buttons to show on toolbar, array of jQuery input/buttons elements

		}
	});

});

function previous() {
$('#smartwizard').smartWizard("prev");
}

function reset() {
$('#smartwizard').smartWizard("reset");
result.clear();
document.getElementById("footer").innerHTML = footer + " NOCH NICHTS AUSGEWÄHLT!";
}

var infoClick = function() {
	$.notify({
		// options
		title: '<strong>Info</strong>',
		message: "<br>Mit diesem Planungstool generieren Sie in wenigen Schritten einen individuellen Leitfaden, der Sie dabei unterstützt qualitativ hochwertige Bildungsmaterialien im OER Standard zu erstellen. Entsprechend Ihrer Auswahl erhalten Sie formale und didaktische Empfehlungen sowie Hinweise zu Lizenzangaben",
		icon: 'glyphicon glyphicon-info-sign',
	}, {
		// settings
		element: 'body',
		position: null,
		type: "info",
		allow_dismiss: true,
		newest_on_top: true,
		showProgressbar: true,
		placement: {
			from: "top",
			align: "center"
		},
		z_index: 1050,
		delay: 60000,
		url_target: '_blank',
		mouse_over: null,
		animate: {
			enter: 'animated flipInY',
			exit: 'animated flipOutX'
		},
		onShow: null,
		onShown: null,
		onClose: null,
		onClosed: null,
		icon_type: 'image',
		template: '<div style="font-size:19px;  font-family: Work Sans, sans-serif;" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
			'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
			'<span data-notify="title">{1}</span>' +
			'<span data-notify="message">{2}</span>' +
			'</div>'

	});
} 
