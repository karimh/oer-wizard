/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
function replay_step_6(clicked_id, id) {

    pdf();
   
    document.getElementById("step-5").style.display = "none";

    document.getElementById("page").setAttribute("style", "min-height: 0px;"); 

    document.getElementById("step-6").innerHTML = "<img style='margin-top: 3%; border-radius: 10px; background-color: #FFFFFF; margin-left: 35%'"+
    "src='../image/wizard/image_7.png' width='300' height='300'>"+
   "<div style='margin-left: 29%; margin-bottom: 10%; font-size: 28px; margin-top: 1%;'>Gutes Gelingen bei der OER Erstellung!</div>";

    $('div[id^="pdfbutton"]').css("display", "none");
    $('div[id^="smartwizard"]').css("display", "none");
    document.getElementById("footer").innerHTML = "";
    document.getElementById("footer").removeAttribute("style", "height: 70px;");
    document.getElementById("modal-content").setAttribute("style", "width: 100%;"); 
    document.getElementById("step-6").style.display = "block";

}