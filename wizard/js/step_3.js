/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var message_Q4 = "<div class='textfild'>Planen Sie in Ihrer Bildungsressource fremde Materialien/Inhalte zu verwenden?</div><br/><br/>"; 
var q4map = new Map();

function replay_step_3(clicked_id,id) {
	
	q4map.set("q3","Ja, ich möchte fremdes Material einbinden")
	q4map.set("q4","Ja, ich möchte fremdes Material verändern und wiederveröffentlichen")
	q4map.set("q1","Nein, ich werde ausschließlich eigene Materialien verwenden")

    	var par = "step-"+id;
		var seit = "s"+id;

		if(button1.size > 0 && button1.get(clicked_id) != undefined){
			setData(clicked_id)
			}
		 if(button2.size > 0 && button2.get(clicked_id) != undefined){
			 setData(clicked_id)
				}
		 if(button3.size > 0 && button3.get(clicked_id) != undefined){
			 //setData(clicked_id)
				}
		 if(button4.size > 0 && button4.get(clicked_id) != undefined){
			 //setData(clicked_id)
				}

	    $('#smartwizard').smartWizard("next");
	    
	    document.getElementById(par).innerHTML = "</br>"+ message_Q4;
	    document.getElementById(seit).innerHTML = id+ " Urheberrechtsbestimmungen";
		
		for (const [key, value] of q4map.entries()) {
			
			if(id == 3){
				
				var temp = "<button id="+ key+ " onClick='replay_step_4(this.id, 4)' " +
					" text='"  +key +
					" buttontext='"  +value.split(":")[0] +
					"' class='button3'> " +value.split(":")[0] +"</button></br></br></br>"

			}else{ 
				
				 	temp = "<button id="+ key+ " onClick='replay_step_4(this.id, 5)' " +
					"text='" +key +
					"'  buttontext='"  +value.split(":")[0] +
					"' class='button3'> " +value.split(":")[0] +"</button></br></br></br>"
			}
			document.getElementById(par).innerHTML += temp;
		}
		
			navigation.set("nav3", document.getElementById(clicked_id).getAttribute("text"));
			document.getElementById("footer").innerHTML = footer + navigation.get("nav1").toUpperCase()+" &#10141; "+navigation.get("nav3").toUpperCase();
			
	}	

  function setData(clicked_id){
	  
		var button1_select = queryId(clicked_id);
		
		var b1_text = button1_select.getAttributeNode("dataformat").value;
		var b1_tool = button1_select.getAttributeNode("tool").value;
		var b1_heading = button1_select.getAttributeNode("heading").value;
		var b1_intro = button1_select.getAttributeNode("intro").value;

		result.set("dataformat", b1_text);
		result.set("tool", b1_tool);
		result.set("heading", b1_heading);
		result.set("intro", b1_intro);
  }
