/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */

function tooltip(tooltipData){
	$(document).ready(function(){
		TOOLTIP_DATA.map((data)=>{
		$("#"+data.id).tooltip({
			title: data.title,
			html: true,
			trigger : 'hover',
		});
	  })
	});
}
