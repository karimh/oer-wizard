/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var knowledge = "Die <a href='https://gitlab.com/TIBHannover/oer/course-metadata-test' target='_blank' rel='noopener noreferrer'> Gitlab Kursvorlage der TIB Hannover </a> kann für die Erstellung von offenen  Bildungsressourcen genutzt werden. Vorteile ergeben sich in der Darstellung  der  Bildungsressourcen  (ebook,  PDF,  HTML)  sowie  der  Einbindung  und  Einbettung anderer Formate";

var message = "</br><center>Teilen<center></br>Vielen Dank, dass Sie sich dazu entschieden haben, eine OER zu erstellen und für andere zugänglich zu machen. Sie können Ihre OER über das OER-Portal mit anderen teilen. Für die Anmeldung im OER-Portal benötigen Sie einen Account, den Sie unter <a href='mailto:support.oer-nds@tib.eu' target='_blank' rel='noopener noreferrer'>support.oer-nds@tib.eu</a> beantragen.";

var cc0map = new Map();

function replay_step_5(clicked_id, co) {

		  tooltip();

		  document.getElementById("s5").innerHTML = "5" + " Ergebnis";

    	  var buttonselect = queryId(clicked_id);
    	  var attr = buttonselect.getAttributeNode("lic").value;

    	  cc0map.clear()

    	  if(clicked_id == "l1"){
    		  cc0map.set("cc0",cc0_a)
    	  }
    	  if(clicked_id == "l2"){
    		  cc0map.set("cc0",cc_by)
    	  }
    	  if(clicked_id == "l3"){
    		  cc0map.set("cc0",cc_by_sa)
    	  }
    	  if(clicked_id == "l4"){
    		  cc0map.set("cc0",cc_by_nd)
    	  }
    	  if(clicked_id == "l5"){
    		  cc0map.set("cc0",cc_by_nc)
    	  }
    	  if(clicked_id == "l6"){
    		  cc0map.set("cc0",cc_by_nc_sa)
    	  }
    	  if(clicked_id == "l7"){
    		  cc0map.set("cc0",cc_by_nc_nd)
    	  }
    	  if(clicked_id == "l8"){
    		  cc0map.set("cc0",cc_pd_mark)
    	  }
    	  if(clicked_id == "l9"){
    		  cc0map.set("cc0",copy1)
    	  }


    	  if(clicked_id == "l10"){
    		  cc0map.set("cc0",cc0_b)
    	  }
    	  if(clicked_id == "l11"){
    		  cc0map.set("cc0",cc_by_4)
    	  }
    	  if(clicked_id == "l12"){
    		  cc0map.set("cc0",cc_by_sa_4)
    	  }
    	  if(clicked_id == "l13"){
    		  cc0map.set("cc0",cc_by_nd_4)
    	  }
    	  if(clicked_id == "l14"){
    		  cc0map.set("cc0",cc_by_nc_4)
    	  }
    	  if(clicked_id == "l15"){
    		  cc0map.set("cc0",cc_by_nc_sa_4)
    	  }
    	  if(clicked_id == "l16"){
    		  cc0map.set("cc0",cc_by_nc_nd_4)
    	  }


    	  result.set("license", cc0map.get("cc0").replaceAll("</br>", "").replaceAll("</ol>","").replaceAll("<ol>","").replaceAll("</li>","").replaceAll("<li>",""))
    	  var copy = "c"+co;


	    	var heading = "<div style='box-shadow: 0px 3px 13px -10px #3f4a4d; padding: 2%; margin: 2%; margin-left: 2%; border-style: solid; border-color: #b1dfbb; border-width: 2px; font-size: 24px; text-align: center; color:#0A1F40; border-radius: 4px;'>" +
	    	  				""+result.get("heading")+
							  "</div></br>";

			let b1 = "<div class='row'><div  class='col-8'><span  class='span'>Schritt 1: Nutzungsbedingungen prüfen"+"</span></div></div></br></br></br></br>"+
						"<div class='row'>"+
								"<div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>"+
										"<img class='imagelable' src='../image/wizard/paragraph.png' width='150' height='150'>" +
								"</div>"+
								"<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>"+
									"<div class='span1'>"+text_2+"</div>"+
								"</div>"+
						"</div></br></br>"+
						"<div class='row'>"+
							"<div class='col-8'>"+
								"<span class='labletext'>"+ result.get(copy).split("•").join("") +"</span>"+
							"</div>"+
						"</div></br></br>";

			let b2 = "<div class='row'><div  class='col-8'><span  class='span'>Schritt 2 Werkzeug wählen"+"</span></div></div></br></br></br></br>"+
					  "<div class='row'>"+
							  "<div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>"+
									"<img class='imagelable' src='../image/wizard/image_3.png' width='150' height='150'>" +
							  "</div>"+
							  "<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>"+
								  "<div class='span1'>"+text_3+"</div>"+
							  "</div>"+
					  "</div></br></br>"+
					  "<div class='row'>"+
						  "<div class='col-8'>"+
							  "<span class='labletext'>"+ result.get("tool").split("•").join("") +"</span>"+
						  "</div>"+
					  "</div></br></br>";

			let b3 = "<div class='row'><div  class='col-8'><span  class='span'>Schritt 3: Formale und didaktische Hinweise beachten"+"</span></div></div></br></br></br></br>"+
					  "<div class='row'>"+
							  "<div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>"+
									"<img class='imagelable' src='../image/wizard/image_1.png' width='150' height='150'>" +
							  "</div>"+
							  "<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>"+
								  "<div class='span1'>"+text_4+"</div>"+
							  "</div>"+
					  "</div></br></br>"+
					  "<div class='row'>"+
						  "<div class='col-8'>"+
							  "<span class='labletext'>"+ result.get("intro").split("•").join("") +"</span>"+
						  "</div>"+
					  "</div></br></br>";

			let b4 = "<div class='row'><div  class='col-8'><span  class='span'>Schritt 4: Lizenzen transparent machen"+"</span></div></div></br></br></br></br>"+
					  "<div class='row'>"+
							  "<div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>"+
									"<img class='imagelable' src='../image/wizard/image_5.png' width='150' height='150'>" +
							  "</div>"+
							  "<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>"+
								  "<div class='span1'>"+text_5+"</div>"+
							  "</div>"+
					  "</div></br></br>"+
					  "<div class='row'>"+
						  "<div class='col-8'>"+
							  "<span class='labletext'>"+ cc0map.get("cc0") +"</span>"+
						  "</div>"+
					  "</div></br></br>";

			let b5 = "<div class='row'><div  class='col-8'><span  class='span'>Schritt 5: Bildungsmaterial speichern"+"</span></div></div></br></br></br></br>"+
					  "<div class='row'>"+
							  "<div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>"+
									"<img class='imagelable' src='../image/wizard/image_4.png' width='150' height='150'>" +
							  "</div>"+
							  "<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>"+
								  "<div class='span1'>"+text_6+"</div>"+
							  "</div>"+
					  "</div></br></br>"+
					  "<div class='row'>"+
						  "<div class='col-8'>"+
							  "<span class='labletext'>"+ result.get("dataformat").split("•").join("") +"</span>"+
						  "</div>"+
					  "</div></br></br>";

			let b6 = "<div class='row'><div  class='col-8'><span  class='span'>Wissenswertes"+"</span></div></div></br></br></br></br>"+
					  "<div class='row'>"+
							  "<div  class='col-xl-2 col-lg-2 col-md-4 col-sm-12'>"+
									 "<img class='imagelable' src='../image/wizard/image_6.png' width='150' height='150'>" +
							  "</div>"+
							  "<div class='col-xl-10 col-lg-10  col-md-8 col-sm-12'>"+
								  "<div class='span1'>"+text_7+"</div>"+
							  "</div>"+
					  "</div></br></br>"+
					  "<div class='row'>" +
							"<div class='col-8'>" +
								"<span class='labletext'>"+ knowledge+"</span>" +
							"</div>" +
					  "</div></br></br></br>" +
					  "<div class='row'>"+
						  "<div class='col-12'>"+
							  "<div style='font-family:sans-serif;' class='message2'><em>"+message+"</em></div>"+
						  "</div>"+
					  "</div></br></br>";

		navigation.set("nav5", attr)
	    document.getElementById("step-5").innerHTML = "</br><div class='statictext'>"+text_1+"</div></br></br>"+"<div class='container'>"+heading+"</br>"+b1+b2+b3+b4+b5+b6+"</div>";
		document.getElementById("footer").setAttribute("style", "height: 80px; float: left; width: 100%; text-align: left;"); 
	    document.getElementById("footer").innerHTML = footer + navigation.get("nav1").toUpperCase()+
	    		" &#10141; " +navigation.get("nav3").toUpperCase()+
	    		" &#10141; " +navigation.get("nav4").toUpperCase()+" &#10141; "+navigation.get("nav5").toUpperCase()+
	    		"<div id='pdfbutton' class='box' ><button style='background-color: #54B6B5; border-radius: 4px; border: 6px solid #54B6B5; " +
	    		"box-shadow: 2px 5px 15px -10px #3f4a4d; color: white; font-size: 17px;' type='button' onClick='replay_step_6(this.id, 5)'>PDF herunterladen</button></div>";

	    $('#smartwizard').smartWizard("next");
   }