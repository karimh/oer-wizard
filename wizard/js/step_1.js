/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var message2 = "<div class='textfild'>Welches Format möchten Sie erstellen?</div><br/>"
var message5 = "<div class='textfild'>Welches Format möchten sie erstellen?</div><br/>"
var message6 = "<div class='textfild'>Welche Lern- Oder Lehreneinheit möchten Sie planen?</div><br/>"
var message7 = "<div class='textfild'>Welcher Lehrart möchten Sie folgen?</div><br/>"

var footer = "<span style='color: #0A1F40; font-size: 13px; text-align: left;'>IHRE AUSWAHL: ";

var button1 = new Map();
var button2 = new Map();
var button3 = new Map();
var button4 = new Map();

let result = new Map();

var navigation = new Map();

function replay_step_1(clicked_id) {

	tooltip();

	button1.set("b1_1", b1_1_array);
	button1.set("b1_2", b1_2_array);
	button1.set("b1_3", b1_3_array);
	button1.set("b1_4", b1_4_array);
	button1.set("b1_5", b1_5_array);
	button1.set("b1_6", b1_6_array);
	button1.set("b1_7", b1_7_array);
	//button1.set("b1_8", b1_8_array);
	button1.set("b1_9", b1_9_array);
	button1.set("b1_10", b1_10_array);
	button1.set("b1_11", b1_11_array);
	button1.set("b1_12", b1_12_array);

	remove(clicked_id)

	if (clicked_id == "step1-b1") {
	
		document.getElementById("step-2").innerHTML = "</br>"+ message2 + "</br>";
		document.getElementById("s2").innerHTML = "2 Format";

		for (const [key, value] of button1.entries()) {

			 let b1 = "<button id="+ key+ " onClick='replay_step_3(this.id, 3)' " +
							   	  "  tool='"+value['tool'] +
							   	  "' heading='"+value['heading'] +
							   	  "' intro='"+value['intro'] +
							   	  "' dataformat='"+value['dataformat'] +
							   	  "' text='"+value['b1_1'] +
							   	  "' class='button2'> " +value['b1_1'] +"</button>"
			  document.getElementById("step-2").innerHTML += b1;
		}
	}

	if (clicked_id == "step1-b2") {
		
		button2.set("b2_1", b2_1_array);
		button2.set("b2_2", b2_2_array);
		button2.set("b2_3", b2_3_array);
		button2.set("b2_4", b2_4_array);
		button2.set("b2_5", b2_5_array);
		button2.set("b2_6", b2_6_array);
		button2.set("b2_7", b2_7_array);
		button2.set("b2_8", b2_8_array);
		//button2.set("b2_9", b2_9_array);


			document.getElementById("step-2").innerHTML = "</br>"+ message5 + "</br>";
			document.getElementById("s2").innerHTML = "2 Format";

			for (const [key, value] of button2.entries()) {

			let b2 = "<button id="+ key+ " onClick='replay_step_3(this.id, 3)' " +
			   	  "  tool='"+value['tool'] +
			   	  "' heading='"+value['heading'] +
			   	  "' intro='"+value['intro'] +
			   	  "' dataformat='"+value['dataformat'] +
			   	  "' text='"+value['b2_1'] +
			   	  "' class='button2'> " +value['b2_1'] +"</button>"
				  document.getElementById("step-2").innerHTML += b2;

			}

	}

	if (clicked_id == "step1-b3") {

		button3.set("b3_1", b3_1_array);
		button3.set("b3_2", b3_2_array);
		button3.set("b3_3", b3_3_array);
		button3.set("b3_4", b3_4_array);
		button3.set("b3_5", b3_5_array);
		button3.set("b3_6", b3_6_array);
		button3.set("b3_7", b3_7_array);
		button3.set("b3_8", b3_8_array);
		button3.set("b3_9", b3_9_array);
		button3.set("b3_10", b3_10_array);
		button3.set("b3_11", b3_11_array);
		button3.set("b3_12", b3_12_array);

		document.getElementById("step-2").innerHTML = "</br>"+ message6 + "</br>";
		document.getElementById("s2").innerHTML = "2 Lehrphasen";

		for (const [key, value] of button3.entries()) {

			/*if the text is ready put -> onClick='replay_step_3(this.id, 3)' */

			
			let b3 = "<button id="+ key+ "  " +
		   	  "  tool='"+value['tool'] +
		   	  "' heading='"+value['heading'] +
		   	  "' intro='"+value['intro'] +
		   	  "' dataformat='"+value['dataformat'] +
		   	  "' text='"+value['b3_1'] +
		   	  "' class='button2'> " +value['b3_1'] +"</button>"
			  document.getElementById("step-2").innerHTML += b3;

		}

	}

	if (clicked_id == "step1-b4") {

		button4.set("b4_1", b4_1_array);
		button4.set("b4_2", b4_2_array);
		button4.set("b4_3", b4_3_array);
		button4.set("b4_4", b4_4_array);

		document.getElementById("step-2").innerHTML = "</br>"+ message7 + "</br>";
		document.getElementById("s2").innerHTML = "2 Lehansatz";

		for (const [key, value] of button4.entries()) {

			/*if the text is ready put -> onClick='replay_step_3(this.id, 3)' */

			let b4 = "<button id="+ key+ "  " +
		   	  "  tool='"+value['tool'] +
		   	  "' heading='"+value['heading'] +
		   	  "' intro='"+value['intro'] +
		   	  "' dataformat='"+value['dataformat'] +
		   	  "' text='"+value['b4_1'] +
		   	  "' class='button2'> " +value['b4_1'] +"</button>"
			  document.getElementById("step-2").innerHTML += b4;
		}
	}

		navigation.set("nav1", document.getElementById(clicked_id).getAttribute("text"));
		document.getElementById("footer").innerHTML = footer
		+navigation.get("nav1").toUpperCase();

	$('#smartwizard').smartWizard("next");

}

	function queryId(clicked_id){
		var id = "#" + clicked_id;
	    var buttonselect = document.querySelector(id);
	    return buttonselect;
	}

	function remove(clicked_id){

		 $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {

			 if(stepDirection === 'backward'){
				$('div[id^="pdfbutton"]').css("display", "none");
				document.getElementById("step-6").style.display = "none";
				document.getElementById("footer").removeAttribute("style", "height: 70px;");
				document.getElementById("footer").setAttribute("style", "float: left; width: 100%; text-align: left;");
				
				 if(stepNumber == 0){
					$('#smartwizard').smartWizard("reset");
					 button1.clear();
					 button2.clear();
					 button3.clear();
					 button4.clear();
				 }
			 }
		 });
	}
