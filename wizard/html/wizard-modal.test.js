/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
import { fireEvent, getByText } from '@testing-library/dom'
import {expect, jest, test} from '@jest/globals';
import '@testing-library/jest-dom/extend-expect'
import { JSDOM } from 'jsdom'
import fs from 'fs'
import path from 'path'
import fetch from 'node-fetch';
jest.mock('node-fetch');
//import {replay_step_1} from '../js/step_1';
const {replay_step_1} = require('../js/step_1')
// jest.mock('./text/object_1.js')


const html = fs.readFileSync(path.resolve(__dirname, './wizard-modal.html'), 'utf8');

let dom
let container

describe('wizard-modal.html', () => {
  beforeEach(() => {
    // Constructing a new JSDOM with this option is the key
    // to getting the code in the script tag to execute.
    // This is indeed dangerous and should only be done with trusted content.
    // https://github.com/jsdom/jsdom#executing-scripts
    dom = new JSDOM(html, { runScripts: 'dangerously' })    
    container = dom.window.document.body
  })

  it('renders a heading element', () => {

    //  expect(replay_step_1("step1-b1")).toEqual("step1-b1")

       expect(getByText(container, 'Welche Bildungsressource möchten Sie erstellen?')).toBeInTheDocument()
  })

  // it('renders a button element', () => {
  //   expect(container.querySelector('button')).not.toBeNull()
  //   expect(getByText(container, 'Kurs')).toBeInTheDocument()
  // })

  // it('renders a new paragraph via JavaScript when the button is clicked', () => {
  //   const button = getByText(container, 'Kurs')
  //   console.log('Hello----'+button.innerHTML)
  //   fireEvent.click(button)

  //   // let generatedParagraphs = container.querySelectorAll('#pun-container p')
  //   // expect(generatedParagraphs.length).toBe(1)

  //   // fireEvent.click(button)
  //   // generatedParagraphs = container.querySelectorAll('#pun-container p')
  //   // expect(generatedParagraphs.length).toBe(2)

  //   // fireEvent.click(button)
  //   // generatedParagraphs = container.querySelectorAll('#pun-container p')
  //   // expect(generatedParagraphs.length).toBe(3)
  //  })
})